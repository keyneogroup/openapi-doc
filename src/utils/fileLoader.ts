import { OpenAPIV3 } from 'openapi-types';
import { Schema, getMediaExample, getRequestExample } from './exampleBuilder';
import { getMediaParameters } from './parametersBuilder';
import { ParameterDescription, ApiDescription } from 'src/@types/types';

const toParametersDescription = (verbDescription: OpenAPIV3.OperationObject, schemas: Schema) => {
    if (verbDescription.parameters?.length) {
        return verbDescription.parameters.map<ParameterDescription>((operation: OpenAPIV3.ParameterObject) => ({
            name: operation.name,
            description: '',
            type: operation.schema.type,
            optional: !operation.required,
        }));
    }
    if (!verbDescription.requestBody?.content) {
        return undefined;
    }
    const result = [getMediaParameters(verbDescription.requestBody.content, schemas)].filter(Boolean);
    return result;
};

const toResponseDescriptions = (verbDescription: OpenAPIV3.OperationObject, showTypes: boolean, schemas: Schema) =>
    verbDescription.responses &&
    [].concat(
        ...Object.entries(verbDescription.responses).map(([code, response]) => {
            const { description, content } = response as OpenAPIV3.ResponseObject;
            return {
                code,
                description,
                content: getMediaExample(content, showTypes, schemas),
            };
        }),
    );

type Methods = 'get' | 'post' | 'put' | 'patch' | 'delete' | 'head' | 'options';
const allowedMethods: Methods[] = ['get', 'post', 'put', 'patch', 'delete', 'head', 'options'];

const toApiDescription = (path: string, description: OpenAPIV3.PathItemObject, schemas: Schema) => {
    return allowedMethods
        .filter(method => !!description[method])
        .map<ApiDescription>(method => {
            const verbDescription = description[method] as OpenAPIV3.OperationObject;
            return {
                path,
                method,
                summary: verbDescription.summary,
                requestBody: getRequestExample(verbDescription.requestBody, true, schemas),
                requestExample: getRequestExample(verbDescription.requestBody, false, schemas),
                parameters: toParametersDescription(verbDescription, schemas),
                responses: toResponseDescriptions(verbDescription, false, schemas),
                showFirstLevelRequest: !verbDescription.parameters?.length,
            };
        });
};

const loadJson = async (path: string) => {
    const response = await fetch(path);
    return (await response.json()) as OpenAPIV3.Document;
};

export const getOpenApiFile = async (path: string) => {
    const openApiDoc = await loadJson(path);

    const result = ([] as ApiDescription[]).concat(
        ...Object.entries(openApiDoc.paths).map(([path, description]) =>
            toApiDescription(path, description, openApiDoc.components.schemas),
        ),
    );

    return result;
};

export const findDescription = (descriptions: ApiDescription[], url: string, method: string) => {
    return descriptions.find(description => description.path === url && description.method === method);
};
