import { OpenAPIV3 } from 'openapi-types';

export type Schema = {
    [key: string]: OpenAPIV3.ReferenceObject | OpenAPIV3.ArraySchemaObject | OpenAPIV3.NonArraySchemaObject;
};

const readValueExample = (description: OpenAPIV3.NonArraySchemaObject, showTypes: boolean) => {
    if (showTypes) return description.type;
    if (description.example) return description.example;
    if (description.type === 'object') return {};
    if (description.type === 'string') return 'string';
    if (description.type === 'integer') return 0;
    if (description.type === 'number') return 0;
    if (description.type === 'boolean') return false;
    return 'unknown type: ' + description.type;
};

const getRefSchemaName = ({ $ref }: OpenAPIV3.ReferenceObject) => /\/(\w+)$/.exec($ref)?.[1];

const getSchemaExample = (
    description: OpenAPIV3.ArraySchemaObject | OpenAPIV3.NonArraySchemaObject | OpenAPIV3.ReferenceObject,
    showTypes: boolean,
    schemas: Schema,
) => {
    if (description['type'] === 'array') {
        const { items } = description as OpenAPIV3.ArraySchemaObject;
        return [getSchemaExample(items, showTypes, schemas)];
    }

    if (description['$ref']) {
        const schema = schemas[getRefSchemaName(description as OpenAPIV3.ReferenceObject)];
        return getSchemaExample(schema, showTypes, schemas);
    }

    if (description['properties']) {
        const { properties, allOf } = description as OpenAPIV3.NonArraySchemaObject;
        const baseValues = allOf
            ? allOf.reduce((result, baseType) => {
                  result = { ...result, ...getSchemaExample(baseType, showTypes, schemas) };
                  return result;
              }, {})
            : {};
        return {
            ...baseValues,
            ...Object.keys(properties).reduce((result, keyName) => {
                result[keyName] = getSchemaExample(properties[keyName], showTypes, schemas);
                return result;
            }, {}),
        };
    }

    if (description['type']) {
        return readValueExample(description as OpenAPIV3.NonArraySchemaObject, showTypes);
    }

    return {};
};

export const getMediaExample = (
    exampleType: { [media: string]: OpenAPIV3.MediaTypeObject },
    showTypes: boolean,
    schemas: Schema,
) => {
    if (!exampleType) {
        return undefined;
    }

    const mediaTypeObject =
        exampleType['application/json'] || exampleType['*/*'] || exampleType['text/plain; charset=utf-8'];
    if (!mediaTypeObject || !mediaTypeObject.schema) {
        return undefined;
    }
    return getSchemaExample(mediaTypeObject.schema, showTypes, schemas);
};

export const getRequestExample = (
    requestBody: OpenAPIV3.ReferenceObject | OpenAPIV3.RequestBodyObject,
    showTypes: boolean,
    schemas: Schema,
) => {
    if (!requestBody) {
        return undefined;
    }
    if (requestBody['$ref']) {
        return getSchemaExample(requestBody as OpenAPIV3.ReferenceObject, showTypes, schemas);
    }
    const { content } = requestBody as OpenAPIV3.RequestBodyObject;
    return getMediaExample(content, showTypes, schemas);
};
