import { OpenAPIV3 } from 'openapi-types';
import { ParameterDescription } from 'src/@types/types';

export type Schema = {
    [key: string]: OpenAPIV3.ReferenceObject | OpenAPIV3.ArraySchemaObject | OpenAPIV3.NonArraySchemaObject;
};

const readValueExample = (description: OpenAPIV3.NonArraySchemaObject): ParameterDescription => {
    return {
        name: description.title,
        description: description.description,
        optional: !description.required,
        type: description.type,
    };
};

const getRefSchemaName = ({ $ref }: OpenAPIV3.ReferenceObject) => /\/(\w+)$/.exec($ref)?.[1];

const getSchemaParameterDescription = (
    description: OpenAPIV3.ArraySchemaObject | OpenAPIV3.NonArraySchemaObject | OpenAPIV3.ReferenceObject,
    schemas: Schema,
): ParameterDescription => {
    if (description['type'] === 'array') {
        const arrayDescription = description as OpenAPIV3.ArraySchemaObject;
        if (!arrayDescription.items['$ref']) {
            console.log(arrayDescription.items);
        }
        const type = getRefSchemaName(arrayDescription.items) || arrayDescription.items.type;
        const descriptions = getSchemaParameterDescription(arrayDescription.items, schemas);
        return {
            name: arrayDescription.title,
            description: arrayDescription.description,
            optional: !arrayDescription.required,
            type: type + '[]',
            subDescriptions: descriptions.subDescriptions,
        };
    }

    if (description['$ref']) {
        const refDescription = description as OpenAPIV3.ReferenceObject;
        const schemaName = getRefSchemaName(refDescription);
        const schema = schemas[schemaName];
        if (schemaName === 'RequestZoneDTO') {
            console.log(description);
        }
        return { ...getSchemaParameterDescription(schema, schemas), type: schemaName, name: description.title };
    }

    if (description['properties']) {
        const { properties, allOf, required } = description as OpenAPIV3.NonArraySchemaObject;
        const values = [
            ...(allOf?.map(baseType => getSchemaParameterDescription(baseType, schemas)) || []),
            ...Object.keys(properties).map(keyName =>
                getSchemaParameterDescription(
                    { ...properties[keyName], title: keyName, required: !required || required.includes(keyName) },
                    schemas,
                ),
            ),
        ];

        return {
            description: description.description,
            name: description.title,
            optional: !description.required,
            type: description.type,
            subDescriptions: values,
        };
    }
    const valueDescription = description as OpenAPIV3.NonArraySchemaObject;
    if (valueDescription.type) {
        return readValueExample(valueDescription);
    }
};

export const getMediaParameters = (
    exampleType: { [media: string]: OpenAPIV3.MediaTypeObject },
    schemas: Schema,
): ParameterDescription => {
    if (!exampleType) {
        return undefined;
    }

    const mediaTypeObject =
        exampleType['application/json'] || exampleType['*/*'] || exampleType['text/plain; charset=utf-8'];
    if (!mediaTypeObject?.schema) {
        return undefined;
    }

    return getSchemaParameterDescription(mediaTypeObject.schema, schemas);
};
