import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import { getOpenApiFile, findDescription } from './utils/fileLoader';
import { ApiDescription } from './@types/types';

const getDocFilePaths = (targets: HTMLCollectionOf<Element>) => {
    const result = [];

    for (let i = 0; i < targets.length; i++) {
        const target = targets.item(i);
        const url = target.getAttribute('data-docfile-path');
        if (!result.includes(url)) {
            result.push(url);
        }
    }

    return result;
};

const main = async () => {
    if (process.env.NODE_ENV !== 'production') {
        const descriptions = await getOpenApiFile('api-docs.json');

        ReactDOM.render(
            <div>
                {descriptions.map(description => (
                    <App key={`${description.path}-${description.method}`} apiDescription={description} />
                ))}
            </div>,
            document.getElementById('root'),
        );
        return;
    }
    const targets = document.getElementsByClassName('endpoint-documentation');

    const docUrls = (
        await Promise.all(
            getDocFilePaths(targets).map(async path => {
                const doc = await getOpenApiFile(path);
                return {
                    doc,
                    path,
                };
            }),
        )
    ).reduce((result, { doc, path }) => {
        result[path] = doc;
        return result;
    }, {} as { [path: string]: ApiDescription[] });

    for (let i = 0; i < targets.length; i++) {
        const target = targets.item(i);
        const url = target.getAttribute('data-url');
        const method = target.getAttribute('data-method');
        const docFilePath = target.getAttribute('data-docfile-path');
        const description = findDescription(docUrls[docFilePath], url, method);
        if (!description) {
            console.log('no description found for ', url, method, docFilePath);
            continue;
        }

        ReactDOM.render(<App apiDescription={description} />, target);
    }
};

window.addEventListener('DOMContentLoaded', main);
