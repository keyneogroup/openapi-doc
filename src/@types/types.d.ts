export interface ApiDescription {
    method: string;
    path: string;
    summary: string;
    requestBody?: {};
    requestExample?: {};
    showFirstLevelRequest: boolean;
    parameters: ParameterDescription[];
    responses: ResponseDescription[];
}

export interface ComponentDescription {
    component: string;
    properties: string;
}

export interface RequestBodyDescription {
    description?: string;
    required?: boolean;
    content: string;
}
export interface ResponseDescription {
    code: string;
    description: string;
    content: {};
}

export interface ParameterDescription {
    name: string;
    type: string;
    description: string;
    optional: boolean;
    subDescriptions?: ParameterDescription[];
}
