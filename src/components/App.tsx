import React, { useEffect } from 'react';
import ApiComponent from './ApiComponent';
import { ApiDescription } from 'src/@types/types';

interface Props {
    apiDescription: ApiDescription;
}

const App: React.FC<Props> = ({ apiDescription }) => {
    useEffect(() => {
        if (window.hljs) {
            document.querySelectorAll('pre code').forEach(block => {
                hljs.highlightBlock(block);
            });
        }
    }, []);

    return (
        <section style={{ margin: '10px 0' }}>
            <ApiComponent description={apiDescription} />
        </section>
    );
};

export default App;
