import React, { useEffect, useRef } from 'react';

export const JsonPresenter: React.FC<{
    data: {};
}> = ({ data }) => {
    const codeRef = useRef<HTMLElement>(null);

    useEffect(() => {
        if (window.hljs) {
            hljs.highlightBlock(codeRef.current);
        }
    }, []);

    return (
        <pre>
            <code
                className="language-json"
                ref={codeRef}
                dangerouslySetInnerHTML={{
                    __html: JSON.stringify(data, null, 2),
                }}
            ></code>
        </pre>
    );
};
