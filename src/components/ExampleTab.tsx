import React from 'react';
import { JsonPresenter } from './JsonPresenter';

interface Props {
    examples: {};
}

export const ExampleTab: React.FC<Props> = ({ examples }) => (
    <div className="" id="tab-get-example" role="tabpanel" aria-labelledby="nav-get-example">
        <JsonPresenter data={examples} />
    </div>
);
