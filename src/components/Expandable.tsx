import React, { useState } from 'react';

export const Expandable: React.FC<{ title?: string }> = ({ children, title }) => {
    const [isOpen, setIsOpen] = useState(false);

    return (
        <div className="expand">
            <div className="expand-label" style={{ cursor: 'pointer' }} onClick={() => setIsOpen(!isOpen)}>
                <i style={{ fontSize: 'x-small', width: '10px' }} className="fas fa-chevron-right" />
                <span>{title || 'Expand'}</span>
            </div>
            {isOpen && <div className="expand-content">{children}</div>}
        </div>
    );
};
