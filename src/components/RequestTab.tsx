import React from 'react';
import { ParameterDescription } from 'src/@types/types';
import { Expandable } from './Expandable';
import { JsonPresenter } from './JsonPresenter';

interface Props {
    parameters: ParameterDescription[];
    requestBody?: {};
    firstLevel: boolean;
}

export const RequestTab: React.FC<Props> = ({ parameters, requestBody, firstLevel }) => (
    <div className="" role="tabpanel" aria-labelledby="nav-get-request">
        {requestBody && <JsonPresenter data={requestBody} />}

        <ParametersTable parameters={parameters} firstLevel={firstLevel} />
    </div>
);

export const ParametersTable: React.FC<{
    parameters: ParameterDescription[];
    firstLevel: boolean;
}> = ({ parameters, firstLevel }) => (
    <table className="api-table" style="width: 100%">
        <thead />
        <tbody>
            {parameters?.map(parameter => (
                <>
                    {!firstLevel && (
                        <tr className="api-table-row">
                            <td className="api-table-cell">
                                {<div className="api-param-name">{parameter.name}</div>}
                                {parameter.optional && <div className="api-param-optional">optional</div>}
                            </td>
                            <td className="api-table-cell">
                                {<samp className="api-param-type">{parameter.type}</samp>}
                            </td>
                            <td className="api-table-cell">
                                <div className="api-param-desc">{parameter.description}</div>
                            </td>
                        </tr>
                    )}
                    {parameter.subDescriptions?.length ? (
                        <tr>
                            <td className="api-table-cell" />
                            <td className="api-table-cell api-table-expand" colSpan="2">
                                {firstLevel ? (
                                    <ParametersTable parameters={parameter.subDescriptions} firstLevel={false} />
                                ) : (
                                    <Expandable>
                                        <ParametersTable parameters={parameter.subDescriptions} firstLevel={false} />
                                    </Expandable>
                                )}
                            </td>
                        </tr>
                    ) : (
                        undefined
                    )}
                </>
            ))}
        </tbody>
    </table>
);
