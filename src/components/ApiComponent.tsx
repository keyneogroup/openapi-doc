import React, { useState } from 'react';
import { RequestTab } from './RequestTab';
import { ResponseTab } from './ResponseTab';
import { ExampleTab } from './ExampleTab';
import { ApiDescription } from 'src/@types/types';

interface Props {
    description: ApiDescription;
}

type ApiTab = 'request' | 'response' | 'example';

const tabTitles = {
    request: 'Request',
    response: 'Response',
    example: 'Example',
};

const ApiComponent: React.FC<Props> = ({ description }) => {
    const hasRequest = description.requestBody || !!description.parameters?.length;
    const [selectedTab, setTab] = useState<ApiTab>(hasRequest ? 'request' : 'response');

    return (
        <div>
            <div className="tabs api">
                <div className="api-head">
                    <span className={`api-method ${description.method}`}>{description.method}</span>
                    <span className="api-path">{description.path}</span>
                    <span className="api-desc">{description.summary}</span>
                </div>

                <div className="tabs-nav" id="tab-nav" role="tablist">
                    {hasRequest && (
                        <button
                            className="tabs-nav-button first"
                            data-toggle="tab"
                            role="tab"
                            aria-controls="tab-get-request"
                            aria-selected={selectedTab === 'request'}
                            onClick={() => setTab('request')}
                        >
                            {tabTitles['request']}
                        </button>
                    )}
                    <button
                        className="tabs-nav-button first"
                        data-toggle="tab"
                        role="tab"
                        aria-controls="tab-get-response"
                        aria-selected={selectedTab === 'response'}
                        onClick={() => setTab('response')}
                    >
                        {tabTitles['response']}
                    </button>

                    {description.requestBody && (
                        <button
                            className="tabs-nav-button first"
                            data-toggle="tab"
                            role="tab"
                            aria-controls="tab-get-example"
                            aria-selected={selectedTab === 'example'}
                            onClick={() => setTab('example')}
                        >
                            {tabTitles['example']}
                        </button>
                    )}

                    <div className="tabs-nav-fill" />
                </div>

                <div className="tabs-content">
                    {selectedTab === 'request' && (
                        <RequestTab
                            parameters={description.parameters}
                            requestBody={description.requestBody}
                            firstLevel={description.showFirstLevelRequest}
                        />
                    )}

                    {selectedTab === 'response' && <ResponseTab statusDescriptions={description.responses} />}

                    {selectedTab === 'example' && <ExampleTab examples={description.requestExample} />}
                </div>
            </div>
        </div>

    );
};
export default ApiComponent;
