import React from 'react';
import { JsonPresenter } from './JsonPresenter';
import { Expandable } from './Expandable';
import { ResponseDescription } from 'src/@types/types';

interface Props {
    statusDescriptions: ResponseDescription[];
}

export const ResponseTab: React.FC<Props> = ({ statusDescriptions }) => (
    <div className="" id="tab-get-response" role="tabpanel" aria-labelledby="nav-get-response">
        {statusDescriptions.map(status => (
            <>
                <div className={`http-code status-${status.code[0]}xx`}>
                    <i className="status-dot fas fa-circle" />
                    <span className="status-code">{status.code}</span>
                    <span className="status-desc">{status.description}</span>
                </div>

                {status.content && (
                    <Expandable title="Content">
                        <JsonPresenter data={status.content} />
                    </Expandable>
                )}
            </>
        ))}
    </div>
);
